function initMap() {
    var styles = [
        {
            "featureType": "landscape",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 45
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 31
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 10
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "road.local",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "lightness": 20
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "featureType": "transit",
            "stylers": [
                {
                    "saturation": -100
                },
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "administrative.province",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "lightness": -45
                },
                {
                    "saturation": -100
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "hue": "#ffff00"
                },
                {
                    "lightness": -45
                },
                {
                    "saturation": -97
                }
            ]
        }
    ];
    var myLatlng = {lat: 54.180431, lng: 45.178803};
    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: myLatlng,
        scrollwheel: false
    });
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');


    var image = new google.maps.MarkerImage( 'img/small-logo2.png',
        new google.maps.Size(200,86),
        new google.maps.Point(0,0),
        new google.maps.Point(42,56)

    );

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'HeadAndHands',
        icon:image
    });


    marker.addListener('click', function() {
        map.setZoom(8);
        map.setCenter(marker.getPosition());
        alert("Привет Из HeadAndHands")
    });
}
document.addEventListener('DOMContentLoaded', function(){
    var button = document.querySelector(".menu-button");
    var menu = document.querySelector(".menu");
    var ChildElements = document.querySelectorAll(".menu a");

    menu.classList.add('hidden');
    button.onclick = function() {
        console.log(ChildElements);
        var height = [].reduce.call(ChildElements, function(sum, current) {
            return sum + current.clientHeight;
        }, 0);
        this.classList.toggle('active');
        menu.style.height = height + 'px';
        menu.classList.toggle('hidden');
    }
    var listContacts = [{
        city: "Москва",
        phone: "+7 (495) 269 84 10",
        point: "ул. Ленинская Слобода, 19<br>Бизнес-центр «Омега плаза»",
        className: "capital1"
    },
    {
        city:"Санкт-Петербург",
        phone: "+ 7 (812) 240-43-35",
        point: "196158, наб. Обводного канала 199,<br>«Обводный двор», офис 3 «А»",
        className: "capital2"
    }
    ];
    var content="";
    listContacts.forEach(function (item) {
        content+="<div class='item "+item.className+"'>" +
                    "<ul>" +
                        "<li itemprop='addressLocality'>"+item.city+"</li>" +
                        "<li><a href='tel:+"+item.phone.replace(/\D/ig, '')+"' itemprop='telephone'>"+item.phone+"</a></li>" +
                        "<li itemprop='addressLocality'>"+item.point+"</li>"+
                    "</ul>" +
                "</div>"

    });
    document.querySelector('.city').innerHTML = content;
});
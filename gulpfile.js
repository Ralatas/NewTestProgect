var gulp = require('gulp');

// Include Our Plugins
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var browserSync    = require('browser-sync').create();
// Compile Our Sass
gulp.task('sass', function() {
  return gulp.src('sass/*.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false}))
      .pipe(sourcemaps.write('./build/css'))
      .pipe(gulp.dest('./build/css'));
});



// Watch Files For Changes
gulp.task('watch', function() {
  // gulp.watch('js/*.js', ['lint', 'scripts']);
  gulp.watch('sass/*.scss', ['sass']);
  gulp.watch('sass/includes/*.scss', ['sass']);
  gulp.watch('jade/*.jade', ['jade']);
  gulp.watch('jade/includes/*.jade', ['jade']);
});

gulp.task('serve', ['sass', 'watch'], function() {
  browserSync.init({
    notify: false,
    server: 'build/',
    startPath: ''
  });
});
// Default Task
gulp.task('default', ['sass', 'watch','serve']);

